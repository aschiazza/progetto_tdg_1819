set N;
set L;
set Linee{L};
set Latt within L;
set Stop{L};
set Logic {L} within{i in N, j in N};
set E within{i in N,j in N};
set S within{u in N,v in N};

param t{L};
param thetanew{N,N,L};
param alpha;
param iu;
param time{S};
param c{E};
param cap;
param Dup{N};
param Ddown{N};

var x{L} binary;
var y{N,N} binary;
var z{N,N,N} binary;
var fn{(u,v) in S, l in L, (i,j) in Logic [l]} binary;
var fr{L} integer;

minimize NumLinee: sum{l in L} t[l]*x[l];
minimize NumTreni: sum{l in L} t[l]*fr[l];

subject to onehop1 {i in N, j in N: i != j}: y[i,j]<=sum{l in L: i in Stop[l] and j in Stop[l]} x[l];   #la variabile y  uguale a 1 solo se esiste una linea che connette i e j, e si ferma in i e j.
subject to onehop2 {i in N, j in N, h in N : h != i and h != j}: z[i,h,j]<= y[i,h]; #la variabile z relativa ad un cammino onehop  uguale a 1 solo se esiste un una linea che connette un semicammino
subject to onehop3 {i in N, j in N, h in N : h != i and h != j}: z[i,h,j]<= y[h,j]; # e deve esistere anche la linea che connette l'altro mezzo cammino

subject to onehop4 {i in N ,j in N: i != j}: y[i,j]+ sum{h in N: h != i and h != j} z[i,h,j]>=1; # impone che ogni coppia di nodi sia raggiungibile o direttamente o al massimo con onehop

subject to flusso {(u,v) in S, i in N: i != v }: sum{l in L, j in N: (i,j) in Logic[l]} fn[u,v,l,i,j] - sum{l in L, j in N: (j,i) in Logic[l]} fn[u,v,l,j,i]=(if i=u then 1 else 0);
subject to flussoattivo{(u,v) in S, l in L, (i,j) in Logic [l]}: fn[u,v,l,i,j]<=x[l];
subject to stretch{(u,v) in S}: sum{l in L, (i,j) in Logic[l]} fn[u,v,l,i,j]*(thetanew[i,j,l]+15)-15<= alpha*time[u,v];
subject to frequenzalb{l in L}: fr[l]>=x[l];
subject to frequenzaub{l in L}: fr[l]<=6*x[l];
subject to domanda{u in N}: sum{l in L: u in Stop[l]} 2*fr[l]*cap>= iu*(Dup[u]+Ddown[u]);
subject to capacita{(u,v) in E}:sum{l in L: u in Linee[l] and v in Linee[l]}fr[l]<=c[u,v];