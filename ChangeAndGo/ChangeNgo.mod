#Dichiarazione Insiemi
set N;
set L;
set Latt within L;
set PathLine{L};
set Stop{L};
set E within {i in N, j in N};
set D within {u in N, v in N};

#Dichiarazione parametri
param time{D};
param q;
param alpha;
param u{E};
param nlmax{L};
param Dup{N};
param Ddown{N};
param beta;
param c{N,N,L};
set ConsecutiveStops{L} within{i in N,j in N};
set ArchPerLine{L} within {i in N, j in N};

#DIchiarazione insieme dei nodi espansi
set VL := {l in L,x in Stop[l]};

#Dichiarazione Insieme degli archi di travel
set Atravel = 
setof {l in L,i in N,j in N : (i,j) in ConsecutiveStops[l]} (l,i,j,0,0) 
	union 
setof {l in L,i in N,j in N : (i,j) in ConsecutiveStops[l]} (l,j,i,0,0)
;

#Dichiarazione Insieme degli archi di Leave.
set Aleave = setof {l in L,i in Stop[l]} (l,i,i,-1,0);

#Dichiarazione Insieme degli archi di Enter
set Aenter = setof {l in L,k in Stop[l], i in 1..nlmax[l]} (l,k,k,1,i);

#Dichiarazione dell'insieme A di tutti gli archi
set A := Atravel union Aleave union Aenter;

#Dichiarazione dell'insieme L2 contenente le coppie di linee che si incrociano 
#(ovvero che hanno almeno una stazione in comune).
set L2 := setof{i in N,l in L, p in L : i in Stop[l] and i in Stop[p] and l!=p} (l,p);

#Dichiarazione dei parametri che contengono i costi relativi ad ogni tipo di arco
param Acost{A};

#Insieme di tutti gli stop di tutte le linee
set SL := setof{l in L, s in Stop[l]} (s);

#Insieme di tutte le linee che passano su un determinato arco
set LE{(e1,e2) in E} := setof{l in L: (e1,e2) in ArchPerLine[l]} (l);

#Dichiarazione variabili
var x{L} binary;
var xf{l in L, 1..nlmax[l]} binary;
var f{(s1,s2) in D, (line,srt,dst,flag,cap) in A} binary;


#Dichiarazione variabili di 1-hop
var xcross{L2} binary;

#Funzione Obiettivo
minimize numTreni: sum{l in L,(s1,s2) in ConsecutiveStops[l]} c[s1,s2,l] * sum{i in 1..nlmax[l]} i * xf[l,i];

#Vincolo che lega la frequenza con l'attivazione della linea, ovvero se una linea � attiva deve avere una frequenza non nulla.
s.t. optModel{l in L}: sum{i in 1..nlmax[l]} xf[l,i] = x[l];

#Vincoli di 1-hop
s.t. oneHop1{(l1,l2) in L2} : xcross[l1,l2] <=x[l1];

s.t. oneHop2{(l1,l2) in L2} : xcross[l1,l2] <=x[l2];

s.t. oneHop3{s1 in N, s2 in N: s1!=s2} : 
	sum{l in L : s1 in Stop[l] and s2 in Stop[l]} x[l] +
	sum{(l1,l2) in L2 : 
		(s1 in Stop[l1] and s2 in Stop[l2]) 
		or (s2 in Stop[l1] and s1 in Stop[l2])} 
		xcross[l1,l2] >=1;

#Vincoli di Domanda
s.t. demand{s in SL} : 2 * q * sum{l in L,i in 1..nlmax[l] : s in Stop[l]} i*xf[l,i] >= beta*(Dup[s]+Ddown[s]);

#Vincoli di Track
s.t. track{(e1,e2) in E} : sum{l in LE[e1,e2],i in 1..nlmax[l]} i* xf[l,i]<= u[e1,e2]; 


#Vincoli di Stretch
s.t. stretch1{(s1,s2) in D,i in N : i!=s1 and i!=s2} :
sum{(line,srt,dst,flag,cap) in A: srt = i} f[s1,s2,line,srt,dst,flag,cap] - 
sum{(line,srt,dst,flag,cap) in A : dst = i} f[s1,s2,line,srt,dst,flag,cap] = 0;

s.t. stretch2{(s1,s2) in D} : 
sum{(line,srt,dst,flag,cap) in A : srt = s1 and (flag=-1)} f[s1,s2,line,srt,dst,flag,cap] -
sum{(line,srt,dst,flag,cap) in A : dst = s1 and (flag=1)} f[s1,s2,line,srt,dst,flag,cap] = 1;

s.t. stretch3{(s1,s2) in D,(line,srt,dst,flag,cap) in A} : f[s1,s2,line,srt,dst,flag,cap] <= 
(if flag =      0 then x[line]
else if flag = -1 then x[line]
else if flag =  1 then xf[line,cap]);

s.t. stretch4{(s1,s2) in D} : 
sum{(line,srt,dst,flag,cap) in A} Acost[line,srt,dst,flag,cap] * 
f[s1,s2,line,srt,dst,flag,cap] <= alpha * time[s1,s2];

